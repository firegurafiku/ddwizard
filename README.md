# ddwizard

Raw disk imaging program with simple graphical user interface for Windows. It may used for the same
purpose as famous Unix `dd` command. This project was originated after I've realized that there is
nothing in Windows world which can take and write simple raw disk images. See [project wiki][1] for
more information about the project.

[1]: https://bitbucket.org/firegurafiku/ddwizard/wiki

## Installation and prerequisites

The program does not have an installer and should properly operate after unpacking to any directory
in file system. Neither additional configuration, nor uninstallation is required too. I've tried to
reduce dependencies to minimum, but, unfortunately, the following is still have to be installed:

* Microsoft Windows XP or newer (I've not tested it on pre-XP systems)
* Microsoft .NET Framework v.3.5 (I'm thinking on going back to 2.0)

## Contribution guidelines

The program is written in C# programming language with Microsoft Visual Studio 2008 Express Edition.
Please, be careful not to 'Upgrade' the project and solution to 'Professional' version, as it would
make them unreadable for users of Express Edition. Any contributions are welcome, even spellchecking.

## Author and license

Copyright © Pavel Kretov, 2014. All rights reserved.  
The program is provided under the terms of [MIT License](http://opensource.org/licenses/MIT).