﻿using System;

namespace DDWizard.DiskUtils
{
    public sealed class PartitionInfo
    {
        public string DeviceID { get; set; }

        public string[] Captions { get; set; }

        public UInt64 Size { get; set; }
    }
}
