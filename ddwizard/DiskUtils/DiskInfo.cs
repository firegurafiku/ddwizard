﻿using System;

namespace DDWizard.DiskUtils
{
    /**
     * <summary>Represents hard disk device possibly holding partitions.</summary>
     * <remarks>This class contains some informational properties of the disk as well as
     * internal device file path and list of partitions located on this disk device. This
     * class does not provide exhausing set of disk properties available to the operating
     * system but the bare minimum.</remarks>
     * <seealso cref="PartitionInfo"/>
     * <seealso cref="DiskInfoRetriever"/>
     */
    public sealed class DiskInfo
    {
        /**
         * <summary>Device file path.</summary>
         * <value>This is the path which can be passed to <c>CreateFile</c> WinAPI
         * function for raw access. It looks like <c>@"\\.\PhysicalDrive0"</c> in
         * current versions of Windows.</value>
         * <seealso cref="PartitionInfo.DeviceID"/>
         */
        public string DeviceID { get; internal set; }

        public string Caption { get; internal set; }

        /**
         * <summary>Device model.</summary>
         * <value>This property contains brief description of hard drive device model
         * firmed in by its manufacturer. For example, it returns <c>"VBOX HARDDISK"</c>
         * on the computer of the original developer. This property may not contain
         * <c>null</c> value.</value>
         */
        public string Model { get; internal set; }

        /**
         * <summary>Device capacity in bytes.</summary>
         * <value>This value may contain zero value in some cases, for example, when
         * disk drive is not accessible for whatever reason.</value>
         */
        public UInt64 Size { get; internal set; }

        /**
         * <summary>List of partitions located on drive.</summary>
         * <value>This list may be empty when operating system recognizes no partitions
         * on the disk drive. This property never returns <c>null</c>.</value>
         * <seealso cref="PartitionInfo"/>
         */
        public PartitionInfo[] Partitions { get; internal set; }
    }
}
