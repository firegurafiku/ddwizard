﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;

namespace DDWizard.DiskUtils
{
    /**
     * @see http://stackoverflow.com/questions/2616378
     */
    public class DiskLocker : IDisposable
    {
        private List<IntPtr> mHandles;

        public DiskLocker(IEnumerable<string> paths)
        {
            mHandles = new List<IntPtr>();
            foreach (var path in paths)
                mHandles.Add(LockVolume(path));
        }

        public void Dispose()
        {
            foreach (var h in mHandles)
            {
                try
                {
                    ReleaseVolume(h);
                }
                finally {
                    WinAPINativeMethods.CloseHandle(h);
                }
            }
        }

        public static IntPtr LockVolume(string fileName)
        {
            IntPtr volumeHandle = WinAPINativeMethods.INVALID_HANDLE_VALUE;
            try
            {
                volumeHandle = WinAPINativeMethods.CreateFile(fileName,
                    FileAccess.Read,
                    FileShare.ReadWrite,
                    IntPtr.Zero,
                    FileMode.Open,
                    (FileAttributes)0,
                    IntPtr.Zero);

                if (volumeHandle == WinAPINativeMethods.INVALID_HANDLE_VALUE)
                    throw new Win32Exception(Marshal.GetLastWin32Error());

                uint dummyNumBytes;
                if (!WinAPINativeMethods.DeviceIoControl(volumeHandle, WinAPINativeMethods.FSCTL_LOCK_VOLUME,
                    IntPtr.Zero, 0, IntPtr.Zero, 0, out dummyNumBytes, IntPtr.Zero))
                    throw new Win32Exception(Marshal.GetLastWin32Error());

                return volumeHandle;
            }
            catch
            {
                WinAPINativeMethods.CloseHandle(volumeHandle);
                throw;
            }
        }

        public static void ReleaseVolume(IntPtr volumeHandle)
        {
            uint dummyNumBytes;
            if (!WinAPINativeMethods.DeviceIoControl(volumeHandle, WinAPINativeMethods.FSCTL_DISMOUNT_VOLUME,
                IntPtr.Zero, 0, IntPtr.Zero, 0, out dummyNumBytes, IntPtr.Zero))
                throw new Win32Exception(Marshal.GetLastWin32Error());

            if (!WinAPINativeMethods.DeviceIoControl(volumeHandle, WinAPINativeMethods.FSCTL_UNLOCK_VOLUME,
                IntPtr.Zero, 0, IntPtr.Zero, 0, out dummyNumBytes, IntPtr.Zero))
                throw new Win32Exception(Marshal.GetLastWin32Error());
        }
    }
}
