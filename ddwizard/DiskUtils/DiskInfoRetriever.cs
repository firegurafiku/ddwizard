﻿using System;
using System.Linq;
using System.Management;

namespace DDWizard.DiskUtils
{
    /// <summary>Class for getting information about disks present in Windows.</summary>
    /// <remarks>The actual information retrieval is performed using Windows Management
    /// Instrumentation (WMI) and hence, it will not work on platforms other than
    /// Microsoft Windows.<remarks>
    /// <seealso cref="DiskInfoRetriever.GetDisks"/>
    public static class DiskInfoRetriever
    {
        /**
         * <summary>Retrieves information about disk devices in system.<summary>
         * <seealso cref="DiskInfo"/>
         * <seealso cref="PartitionInfo"/>
         */
        public static DiskInfo[] GetDisks()
        {
            // TODO: Optimize querying.
            var searcher = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive");
            return searcher.Get()
                .OfType<ManagementObject>()
                .Select(disk => new DiskInfo() {
                    DeviceID = (string)disk["DeviceID"],
                    Caption  = (string)disk["Caption"] ?? "",
                    Model    = (string)disk["Model"] ?? "",
                    Size     = (UInt64?)disk["Size"] ?? 0,
                    Partitions = disk.GetRelated("Win32_DiskPartition")
                        .OfType<ManagementObject>()
                        .Select(part => new PartitionInfo() {
                            DeviceID = string.Format("{0}\\PARTITION{1}",
                                (string)disk["DeviceID"],
                                (UInt32)part["Index"]),
                            Size     = (UInt64?)part["Size"] ?? 0,
                            Captions = part.GetRelated("Win32_LogicalDisk")
                                .OfType<ManagementObject>()
                                .Select(volume => (string)volume["Caption"])
                                .ToArray()
                        }).ToArray(),
                }).ToArray();
        }
    }
}
