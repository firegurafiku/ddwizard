﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DDWizard.DiskUtils;

namespace DDWizard
{
    internal struct WorkerArgs
    {
        public string SourceFileName;
        public string TargetFileName;
        public List<string> VolumesToLock;
    }

    internal struct StepRecord
    {
        public Button Button;
        public Action AfterEntering;
        public Func<bool> BeforeLeaving;

        public StepRecord(Button button, Action entering, Func<bool> leaving)
        {
            Button = button;
            AfterEntering = entering;
            BeforeLeaving = leaving;
        }
    }

    public partial class MainForm : Form
    {
        private List<StepRecord> mStepRecords;

        public MainForm()
        {
            InitializeComponent();
            mStepRecords = new List<StepRecord>() {
                new StepRecord(uxStepSourceButton,  SourcePageEntering, SourcePageLeaving),
                new StepRecord(uxStepDestButton, DestPageEntering, DestPageLeaving),
                new StepRecord(uxStepOptionsButton, OptionsPageEntering, null),
                new StepRecord(uxStepProgressButton, null, null)
            };

            // Some additional steps to ensure correct interface behavior
            // and appearance from the very beginning.
            uxTabs.SelectedIndex = 0;
            ColorizeStepButtons();
            uxStartButton.Visible = false;
            uxPrevButton.Enabled = false;
            uxSourceDeviceRadio.Checked = false;
            uxSourceDeviceRadio.Checked = true;
            uxDestImageRadio.Checked = false;
            uxDestImageRadio.Checked = true;

            // This will take two lookups instead of one.
            RefreshDiskList(uxSourceDeviceList);
            RefreshDiskList(uxDestDeviceList);
        }

        private void ColorizeStepButtons()
        {
            Color fore;
            Color back;

            int index = 0;
            foreach (var rec in mStepRecords)
            {
                if (index == uxTabs.SelectedIndex)
                {
                    fore = uxStepsLayout.BackColor;
                    back = uxStepsLayout.ForeColor;
                }
                else
                {
                    fore = uxStepsLayout.ForeColor;
                    back = uxStepsLayout.BackColor;
                }

                Button button = rec.Button;
                button.ForeColor = fore;
                button.BackColor = back;
                button.FlatAppearance.BorderColor = back;
                button.FlatAppearance.CheckedBackColor = back;
                button.FlatAppearance.MouseDownBackColor = back;
                button.FlatAppearance.MouseOverBackColor = back;

                index++;
            }
        }

        private void SetTabIndex(int index)
        {
            var oldRecord = mStepRecords[uxTabs.SelectedIndex];
            var newRecord = mStepRecords[index];
            bool advance = index > uxTabs.SelectedIndex;

            if (advance && oldRecord.BeforeLeaving != null)
            {
                bool okay = oldRecord.BeforeLeaving();
                if (!okay)
                    return;
            }

            if (newRecord.AfterEntering != null)
                newRecord.AfterEntering();

            uxTabs.SelectedIndex = index;
            ColorizeStepButtons();
        }

        private void RefreshDiskList(ListView view)
        {
            view.BeginUpdate();
            view.Items.Clear();

            foreach (var disk in DiskInfoRetriever.GetDisks()
                .OrderBy(disk => disk.DeviceID))
            {
                var item = new ListViewItem(new string[] {
                    string.Format("{0} [{1}]", disk.DeviceID, disk.Caption),
                    string.Format("{0} MB", disk.Size / 1024 / 1024),
                });

                item.Tag = disk;
                view.Items.Add(item);
            }

            view.EndUpdate();
        }

        private void SourcePageEntering()
        {
            uxPrevButton.Enabled = false;
        }

        private bool SourcePageLeaving()
        {
            if (uxSourceDeviceRadio.Checked &&
                uxSourceDeviceList.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please, select device from the list before proceeding.");
                return false;
            }

            if (uxSourceImageRadio.Checked &&
                !System.IO.File.Exists(uxSourceImageFileTextBox.Text))
            {
                MessageBox.Show("Wrong image file name is entered or file does not exist.");
                return false;
            }

            return true;
        }

        private void DestPageEntering()
        {
            uxBottomLayout.SuspendLayout();
            uxPrevButton.Enabled = true;
            uxNextButton.Visible = true;
            uxStartButton.Visible = false;
            uxBottomLayout.ResumeLayout();
        }

        private bool DestPageLeaving()
        {
            if (uxDestDeviceRadio.Checked &&
                uxDestDeviceList.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please, select device from the list before proceeding.");
                return false;
            }

            if (uxDestImageRadio.Checked && (
                string.IsNullOrEmpty(uxDestImageFileTextBox.Text) ||
                uxDestImageFileTextBox.Text.IndexOfAny(Path.GetInvalidPathChars()) >= 0))
            {
                MessageBox.Show("Wrong image file name is entered or file does not exist.");
                return false;
            }

            return true;
        }

        private void OptionsPageEntering()
        {
            uxBottomLayout.SuspendLayout();
            uxStartButton.Visible = true;
            uxNextButton.Visible = false;
            uxBottomLayout.ResumeLayout();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void PrevButton_Click(object sender, EventArgs e)
        {
            SetTabIndex(uxTabs.SelectedIndex - 1);
        }

        private void NextButton_Click(object sender, EventArgs e)
        {
            SetTabIndex(uxTabs.SelectedIndex + 1);
        }

        private void SourceImageRadio_CheckedChanged(object sender, EventArgs e)
        {
            uxSourceBrowseButton.Enabled = uxSourceImageRadio.Checked;
            uxSourceImageFileTextBox.Enabled = uxSourceImageRadio.Checked;
        }

        private void SourceDeviceRadio_CheckedChanged(object sender, EventArgs e)
        {
            uxSourceRefreshButton.Enabled = uxSourceDeviceRadio.Checked;
            uxSourceDeviceList.Enabled = uxSourceDeviceRadio.Checked;
        }

        private void DestImageRadio_CheckedChanged(object sender, EventArgs e)
        {
            uxDestBrowseButton.Enabled = uxDestImageRadio.Checked;
            uxDestImageFileTextBox.Enabled = uxDestImageRadio.Checked;
        }

        private void DestDeviceRadio_CheckedChanged(object sender, EventArgs e)
        {
            uxDestRefreshButton.Enabled = uxDestDeviceRadio.Checked;
            uxDestDeviceList.Enabled = uxDestDeviceRadio.Checked;
        }

        private void SourceRefreshButton_Click(object sender, EventArgs e)
        {
            RefreshDiskList(uxSourceDeviceList);
        }

        private void DestRefreshButton_Click(object sender, EventArgs e)
        {
            RefreshDiskList(uxDestDeviceList);
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            string sourceFileName = uxSourceImageRadio.Checked
                ? uxSourceImageFileTextBox.Text
                : (uxSourceDeviceList.SelectedItems[0].Tag as DiskInfo).DeviceID;

            string targetFileName = uxDestImageRadio.Checked
                ? uxDestImageFileTextBox.Text
                : (uxDestDeviceList.SelectedItems[0].Tag as DiskInfo).DeviceID;

            uxBottomLayout.SuspendLayout();
            uxPrevButton.Visible = false;
            uxNextButton.Visible = false;
            uxStartButton.Visible = false;
            uxBottomLayout.ResumeLayout();

            uxProgressSourceFileLabel.Text = sourceFileName;
            uxProgressTargetFileLabel.Text = targetFileName;
            SetTabIndex(uxTabs.TabPages.IndexOf(uxProgressPage));

            List<string> volumesToLock = new List<string>();
            if (!uxDestImageRadio.Checked)
            {
                DiskInfo disk = uxDestDeviceList.SelectedItems[0].Tag as DiskInfo;
                foreach (var part in disk.Partitions)
                {
                    foreach (var vol in part.Captions)
                        volumesToLock.Add(string.Format(@"\\.\{0}", vol));
                }
            }

            uxBackgroundWorker.RunWorkerAsync(new WorkerArgs() {
                SourceFileName = sourceFileName,
                TargetFileName = targetFileName,
                VolumesToLock  = volumesToLock,
            });
        }

        private void SourceBrowseButton_Click(object sender, EventArgs e)
        {
            if (uxOpenFileDialog.ShowDialog(this) == DialogResult.OK)
                uxSourceImageFileTextBox.Text = uxOpenFileDialog.FileName;
        }

        private void DestBrowseButton_Click(object sender, EventArgs e)
        {
            if (uxSaveFileDialog.ShowDialog(this) == DialogResult.OK)
                uxDestImageFileTextBox.Text = uxSaveFileDialog.FileName;
        }

        private void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            WorkerArgs arg = (WorkerArgs)e.Argument;
            e.Result = null;

            try
            {
                using (var locker = new DiskLocker(arg.VolumesToLock))
                {
                    DataCopier.Copy(arg.SourceFileName, arg.TargetFileName, status => {
                        uxBackgroundWorker.ReportProgress(0, status);
                    });
                }
            }
            catch (Exception exception)
            {
                e.Result = exception;
            }
        }

        private void BackgroundWorker_RunWorkerCompleted(
            object sender, RunWorkerCompletedEventArgs e)
        {
            uxCancelButton.Text = "Exit";

            if (e.Result == null)
            {
                uxProgressStatusLabel.Text = "Success";
                uxProgressErrorLabel.Text = "None";
            }
            else
            {
                uxProgressStatusLabel.Text = "Failure";
                uxProgressErrorLabel.Text = (e.Result as Exception).Message;
            }
        }

        private void BackgroundWorker_ProgressChanged(
            object sender, ProgressChangedEventArgs e)
        {
            DataCopierStatus status = (DataCopierStatus)e.UserState;
            uxProgressBytesReadLabel.Text =
                string.Format("({0})", status.BytesRead);
            uxProgressBytesWrittenLabel.Text =
                string.Format("({0})", status.BytesWritten);
            uxProgressMegabytesReadLabel.Text =
                string.Format("{0} MB", status.BytesRead / 1024 / 1024);
            uxProgressMegabytesWrittenLabel.Text =
                string.Format("{0} MB", status.BytesWritten / 1024 / 1024);
        }

        private void AboutLinkLabel_LinkClicked(
            object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(uxAboutLinkLabel.Text);
        }
    }
}
