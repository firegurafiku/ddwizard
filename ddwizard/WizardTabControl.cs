﻿using System;
using System.Windows.Forms;

namespace DDWizard
{
    public class WizardTabControl : TabControl
    {
        private bool mCancelSelecting = true;

        public new int SelectedIndex
        {
            get { return base.SelectedIndex; }
            set
            {
                mCancelSelecting = false;
                base.SelectedIndex = value;
                mCancelSelecting = true;
            }
        }

        protected override void WndProc(ref Message m)
        {
            // Hide tabs by trapping the TCM_ADJUSTRECT message.
            if (m.Msg == 0x1328 && !DesignMode) m.Result = (IntPtr)1;
            else base.WndProc(ref m);
        }

        protected override void OnSelecting(TabControlCancelEventArgs e)
        {
            if (DesignMode) base.OnSelecting(e);
            else e.Cancel = mCancelSelecting;
        }
    }
}
