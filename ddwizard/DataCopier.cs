﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;

namespace DDWizard
{
    public struct DataCopierStatus
    {
        public Int64 BytesRead;
        public Int64 BytesWritten;
    }

    public class DataCopier
    {
        private const int BufferSize = 1 * 1024 * 1024;

        public static void Copy(
            string sourceFileName,
            string targetFileName,
            Action<DataCopierStatus> callback)
        {
            bool targetIsDevice = targetFileName.StartsWith(@"\\.\");
            FileMode targetMode = targetIsDevice
                ? FileMode.Open
                : FileMode.Create;

            IntPtr sourceHandle = WinAPINativeMethods.INVALID_HANDLE_VALUE;
            IntPtr targetHandle = WinAPINativeMethods.INVALID_HANDLE_VALUE;
            try
            {
                sourceHandle = WinAPINativeMethods.CreateFile(sourceFileName,
                    FileAccess.Read, FileShare.Read, IntPtr.Zero,
                    FileMode.Open, (FileAttributes)0, IntPtr.Zero);
                if (sourceHandle == WinAPINativeMethods.INVALID_HANDLE_VALUE)
                    throw new Win32Exception(Marshal.GetLastWin32Error());

                targetHandle = WinAPINativeMethods.CreateFile(targetFileName,
                    FileAccess.Write, FileShare.Write, IntPtr.Zero,
                    targetMode, (FileAttributes)0, IntPtr.Zero);
                if (targetHandle == WinAPINativeMethods.INVALID_HANDLE_VALUE)
                    throw new Win32Exception(Marshal.GetLastWin32Error());

                Int64 readBytes = 0;
                Int64 writtenBytes = 0;
                UInt32 lastReadBytes;
                UInt32 lastWrittenBytes;
                byte[] buf = new byte[BufferSize];
                do
                {
                    if (!WinAPINativeMethods.ReadFile(sourceHandle, buf,
                        BufferSize, out lastReadBytes, IntPtr.Zero))
                        throw new Win32Exception(Marshal.GetLastWin32Error());

                    if (!WinAPINativeMethods.WriteFile(targetHandle, buf,
                        lastReadBytes, out lastWrittenBytes, IntPtr.Zero))
                        throw new Win32Exception(Marshal.GetLastWin32Error());

                    readBytes += lastReadBytes;
                    writtenBytes += lastWrittenBytes;

                    if (callback != null)
                    {
                        callback(new DataCopierStatus() {
                            BytesRead = readBytes,
                            BytesWritten = writtenBytes,
                        });
                    }
                }
                while (lastReadBytes != 0 && lastWrittenBytes != 0);
            }
            finally
            {
                WinAPINativeMethods.CloseHandle(sourceHandle);
                WinAPINativeMethods.CloseHandle(targetHandle);
            }
        }
    }
}
