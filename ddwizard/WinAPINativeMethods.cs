﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;

namespace DDWizard
{
    public static class WinAPINativeMethods
    {
        public const uint FSCTL_LOCK_VOLUME = 0x00090018;
        public const uint FSCTL_UNLOCK_VOLUME = 0x0009001c;
        public const uint FSCTL_DISMOUNT_VOLUME = 0x00090020;
        public static readonly IntPtr INVALID_HANDLE_VALUE = new IntPtr(-1);

        [DllImport("kernel32.dll",
            ExactSpelling = true,
            SetLastError = true,
            CharSet = CharSet.Auto)]
        public static extern bool DeviceIoControl(
            IntPtr hDevice,
            uint dwIoControlCode,
            IntPtr lpInBuffer,
            uint nInBufferSize,
            IntPtr lpOutBuffer,
            uint nOutBufferSize,
            out uint lpBytesReturned,
            IntPtr lpOverlapped);

        [DllImport("kernel32.dll")]
        public static extern bool SetFilePointerEx(
            IntPtr hFile, long liDistanceToMove,
            out long lpNewFilePointer, uint dwMoveMethod);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern IntPtr CreateFile(
            [MarshalAs(UnmanagedType.LPTStr)] string filename,
            [MarshalAs(UnmanagedType.U4)] FileAccess access,
            [MarshalAs(UnmanagedType.U4)] FileShare share,
            IntPtr securityAttributes,
            [MarshalAs(UnmanagedType.U4)] FileMode creationDisposition,
            [MarshalAs(UnmanagedType.U4)] FileAttributes flagsAndAttributes,
            IntPtr templateFile);

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CloseHandle(IntPtr hObject);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool ReadFile(IntPtr hFile,
            [Out] byte[] lpBuffer,
            uint nNumberOfBytesToRead,
            out uint lpNumberOfBytesRead,
            IntPtr lpOverlapped);

        [DllImport("kernel32.dll")]
        public static extern bool WriteFile(
            IntPtr hFile,
            byte[] lpBuffer,
            uint nNumberOfBytesToWrite, out uint lpNumberOfBytesWritten,
            IntPtr lpOverlapped);

        public static Int64 GetFileSize(IntPtr hFile)
        {
            const uint Begin = 0;
            const uint Current = 1;
            const uint End = 2;

            Int64 currentPosition;
            if (!SetFilePointerEx(hFile, 0, out currentPosition, Current))
                throw new Win32Exception(Marshal.GetLastWin32Error());

            Int64 result;
            if (!SetFilePointerEx(hFile, -1, out result, End))
                throw new Win32Exception(Marshal.GetLastWin32Error());

            if (!SetFilePointerEx(hFile, currentPosition, out currentPosition, Begin))
                throw new Win32Exception(Marshal.GetLastWin32Error());

            return result;
        }
    }
}