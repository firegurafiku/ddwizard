﻿namespace DDWizard
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("");
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("");
            this.uxStepSourceButton = new System.Windows.Forms.Button();
            this.uxStepDestButton = new System.Windows.Forms.Button();
            this.uxStepOptionsButton = new System.Windows.Forms.Button();
            this.uxStepProgressButton = new System.Windows.Forms.Button();
            this.uxStepsLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.uxBottomLayout = new System.Windows.Forms.TableLayoutPanel();
            this.uxAboutLinkLabel = new System.Windows.Forms.LinkLabel();
            this.uxPrevButton = new System.Windows.Forms.Button();
            this.uxNextButton = new System.Windows.Forms.Button();
            this.uxStartButton = new System.Windows.Forms.Button();
            this.uxCancelButton = new System.Windows.Forms.Button();
            this.uxBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.uxOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.uxSaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.uxTabs = new DDWizard.WizardTabControl();
            this.uxSourceTab = new System.Windows.Forms.TabPage();
            this.uxSourceGroupBox = new System.Windows.Forms.GroupBox();
            this.uxSourceLayout = new System.Windows.Forms.TableLayoutPanel();
            this.uxSourceImageRadio = new System.Windows.Forms.RadioButton();
            this.uxSourceDeviceRadio = new System.Windows.Forms.RadioButton();
            this.uxSourceBrowseButton = new System.Windows.Forms.Button();
            this.uxSourceImageFileTextBox = new System.Windows.Forms.TextBox();
            this.uxSourceDeviceList = new System.Windows.Forms.ListView();
            this.uxSourceNameColumnHeader = new System.Windows.Forms.ColumnHeader();
            this.uxSourceSizeColumnHeader = new System.Windows.Forms.ColumnHeader();
            this.uxSourceRefreshButton = new System.Windows.Forms.Button();
            this.uxDestTab = new System.Windows.Forms.TabPage();
            this.uxDestGroupBox = new System.Windows.Forms.GroupBox();
            this.uxDestLayout = new System.Windows.Forms.TableLayoutPanel();
            this.uxDestImageRadio = new System.Windows.Forms.RadioButton();
            this.uxDestDeviceRadio = new System.Windows.Forms.RadioButton();
            this.uxDestBrowseButton = new System.Windows.Forms.Button();
            this.uxDestImageFileTextBox = new System.Windows.Forms.TextBox();
            this.uxDestDeviceList = new System.Windows.Forms.ListView();
            this.uxDestNameColumnHeader = new System.Windows.Forms.ColumnHeader();
            this.uxDestSizeColumnHeader = new System.Windows.Forms.ColumnHeader();
            this.uxDestRefreshButton = new System.Windows.Forms.Button();
            this.uxOptionsPage = new System.Windows.Forms.TabPage();
            this.uxOptionsGroupBox = new System.Windows.Forms.GroupBox();
            this.uxOptionsNotImplementedYetLabel = new System.Windows.Forms.Label();
            this.uxProgressPage = new System.Windows.Forms.TabPage();
            this.uxProgressGroupBox = new System.Windows.Forms.GroupBox();
            this.uxProgressLayout = new System.Windows.Forms.TableLayoutPanel();
            this.uxProgressSourceFileCaptionLabel = new System.Windows.Forms.Label();
            this.uxProgressMegabytesReadLabel = new System.Windows.Forms.Label();
            this.uxProgressBytesReadLabel = new System.Windows.Forms.Label();
            this.uxProgressTargetFileCaptionLabel = new System.Windows.Forms.Label();
            this.uxProgressDataReadCaption = new System.Windows.Forms.Label();
            this.uxProgressTargetFileLabel = new System.Windows.Forms.Label();
            this.uxProgressSourceFileLabel = new System.Windows.Forms.Label();
            this.uxProgressDataWrittenCaption = new System.Windows.Forms.Label();
            this.uxProgressMegabytesWrittenLabel = new System.Windows.Forms.Label();
            this.uxProgressBytesWrittenLabel = new System.Windows.Forms.Label();
            this.uxProgressStatusCaption = new System.Windows.Forms.Label();
            this.uxProgressStatusLabel = new System.Windows.Forms.Label();
            this.uxProgressErrorCaption = new System.Windows.Forms.Label();
            this.uxProgressErrorLabel = new System.Windows.Forms.Label();
            this.uxStepsLayout.SuspendLayout();
            this.uxBottomLayout.SuspendLayout();
            this.uxTabs.SuspendLayout();
            this.uxSourceTab.SuspendLayout();
            this.uxSourceGroupBox.SuspendLayout();
            this.uxSourceLayout.SuspendLayout();
            this.uxDestTab.SuspendLayout();
            this.uxDestGroupBox.SuspendLayout();
            this.uxDestLayout.SuspendLayout();
            this.uxOptionsPage.SuspendLayout();
            this.uxOptionsGroupBox.SuspendLayout();
            this.uxProgressPage.SuspendLayout();
            this.uxProgressGroupBox.SuspendLayout();
            this.uxProgressLayout.SuspendLayout();
            this.SuspendLayout();
            //
            // uxStepSourceButton
            //
            this.uxStepSourceButton.AutoSize = true;
            this.uxStepSourceButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.uxStepSourceButton.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.uxStepSourceButton.FlatAppearance.BorderSize = 0;
            this.uxStepSourceButton.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.uxStepSourceButton.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.uxStepSourceButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uxStepSourceButton.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.uxStepSourceButton.Image = global::DDWizard.Properties.Resources.num1_filled_32;
            this.uxStepSourceButton.Location = new System.Drawing.Point(0, 0);
            this.uxStepSourceButton.Margin = new System.Windows.Forms.Padding(0);
            this.uxStepSourceButton.Name = "uxStepSourceButton";
            this.uxStepSourceButton.Padding = new System.Windows.Forms.Padding(4, 4, 0, 4);
            this.uxStepSourceButton.Size = new System.Drawing.Size(93, 46);
            this.uxStepSourceButton.TabIndex = 1;
            this.uxStepSourceButton.TabStop = false;
            this.uxStepSourceButton.Text = "  Source";
            this.uxStepSourceButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.uxStepSourceButton.UseVisualStyleBackColor = false;
            //
            // uxStepDestButton
            //
            this.uxStepDestButton.AutoSize = true;
            this.uxStepDestButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.uxStepDestButton.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.uxStepDestButton.FlatAppearance.BorderSize = 0;
            this.uxStepDestButton.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.uxStepDestButton.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.uxStepDestButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uxStepDestButton.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.uxStepDestButton.Image = global::DDWizard.Properties.Resources.num2_filled_32;
            this.uxStepDestButton.Location = new System.Drawing.Point(93, 0);
            this.uxStepDestButton.Margin = new System.Windows.Forms.Padding(0);
            this.uxStepDestButton.Name = "uxStepDestButton";
            this.uxStepDestButton.Padding = new System.Windows.Forms.Padding(4, 4, 0, 4);
            this.uxStepDestButton.Size = new System.Drawing.Size(112, 46);
            this.uxStepDestButton.TabIndex = 1;
            this.uxStepDestButton.TabStop = false;
            this.uxStepDestButton.Text = "  Destination";
            this.uxStepDestButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.uxStepDestButton.UseVisualStyleBackColor = false;
            //
            // uxStepOptionsButton
            //
            this.uxStepOptionsButton.AutoSize = true;
            this.uxStepOptionsButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.uxStepOptionsButton.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.uxStepOptionsButton.FlatAppearance.BorderSize = 0;
            this.uxStepOptionsButton.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.uxStepOptionsButton.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.uxStepOptionsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uxStepOptionsButton.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.uxStepOptionsButton.Image = global::DDWizard.Properties.Resources.num3_filled_32;
            this.uxStepOptionsButton.Location = new System.Drawing.Point(205, 0);
            this.uxStepOptionsButton.Margin = new System.Windows.Forms.Padding(0);
            this.uxStepOptionsButton.Name = "uxStepOptionsButton";
            this.uxStepOptionsButton.Padding = new System.Windows.Forms.Padding(4, 4, 0, 4);
            this.uxStepOptionsButton.Size = new System.Drawing.Size(95, 46);
            this.uxStepOptionsButton.TabIndex = 1;
            this.uxStepOptionsButton.TabStop = false;
            this.uxStepOptionsButton.Text = "  Options";
            this.uxStepOptionsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.uxStepOptionsButton.UseVisualStyleBackColor = false;
            //
            // uxStepProgressButton
            //
            this.uxStepProgressButton.AutoSize = true;
            this.uxStepProgressButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.uxStepProgressButton.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.uxStepProgressButton.FlatAppearance.BorderSize = 0;
            this.uxStepProgressButton.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.uxStepProgressButton.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.uxStepProgressButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uxStepProgressButton.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.uxStepProgressButton.Image = global::DDWizard.Properties.Resources.num4_filled_32;
            this.uxStepProgressButton.Location = new System.Drawing.Point(300, 0);
            this.uxStepProgressButton.Margin = new System.Windows.Forms.Padding(0);
            this.uxStepProgressButton.Name = "uxStepProgressButton";
            this.uxStepProgressButton.Padding = new System.Windows.Forms.Padding(4, 4, 0, 4);
            this.uxStepProgressButton.Size = new System.Drawing.Size(100, 46);
            this.uxStepProgressButton.TabIndex = 1;
            this.uxStepProgressButton.TabStop = false;
            this.uxStepProgressButton.Text = "  Progress";
            this.uxStepProgressButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.uxStepProgressButton.UseVisualStyleBackColor = false;
            //
            // uxStepsLayout
            //
            this.uxStepsLayout.AutoSize = true;
            this.uxStepsLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.uxStepsLayout.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.uxStepsLayout.Controls.Add(this.uxStepSourceButton);
            this.uxStepsLayout.Controls.Add(this.uxStepDestButton);
            this.uxStepsLayout.Controls.Add(this.uxStepOptionsButton);
            this.uxStepsLayout.Controls.Add(this.uxStepProgressButton);
            this.uxStepsLayout.Dock = System.Windows.Forms.DockStyle.Top;
            this.uxStepsLayout.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.uxStepsLayout.Location = new System.Drawing.Point(0, 0);
            this.uxStepsLayout.Name = "uxStepsLayout";
            this.uxStepsLayout.Size = new System.Drawing.Size(640, 46);
            this.uxStepsLayout.TabIndex = 3;
            //
            // uxBottomLayout
            //
            this.uxBottomLayout.AutoSize = true;
            this.uxBottomLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.uxBottomLayout.ColumnCount = 6;
            this.uxBottomLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.uxBottomLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.uxBottomLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.uxBottomLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.uxBottomLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.uxBottomLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.uxBottomLayout.Controls.Add(this.uxAboutLinkLabel, 0, 0);
            this.uxBottomLayout.Controls.Add(this.uxPrevButton, 2, 0);
            this.uxBottomLayout.Controls.Add(this.uxNextButton, 3, 0);
            this.uxBottomLayout.Controls.Add(this.uxStartButton, 4, 0);
            this.uxBottomLayout.Controls.Add(this.uxCancelButton, 5, 0);
            this.uxBottomLayout.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uxBottomLayout.Location = new System.Drawing.Point(0, 383);
            this.uxBottomLayout.Name = "uxBottomLayout";
            this.uxBottomLayout.RowCount = 1;
            this.uxBottomLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.uxBottomLayout.Size = new System.Drawing.Size(640, 29);
            this.uxBottomLayout.TabIndex = 6;
            //
            // uxAboutLinkLabel
            //
            this.uxAboutLinkLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.uxAboutLinkLabel.AutoSize = true;
            this.uxAboutLinkLabel.Location = new System.Drawing.Point(3, 0);
            this.uxAboutLinkLabel.Name = "uxAboutLinkLabel";
            this.uxAboutLinkLabel.Size = new System.Drawing.Size(209, 29);
            this.uxAboutLinkLabel.TabIndex = 4;
            this.uxAboutLinkLabel.TabStop = true;
            this.uxAboutLinkLabel.Text = "https://bitbucket.org/firegurafiku/ddwizard";
            this.uxAboutLinkLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uxAboutLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.AboutLinkLabel_LinkClicked);
            //
            // uxPrevButton
            //
            this.uxPrevButton.Location = new System.Drawing.Point(319, 3);
            this.uxPrevButton.Name = "uxPrevButton";
            this.uxPrevButton.Size = new System.Drawing.Size(75, 23);
            this.uxPrevButton.TabIndex = 1;
            this.uxPrevButton.Text = "Prev";
            this.uxPrevButton.UseVisualStyleBackColor = true;
            this.uxPrevButton.Click += new System.EventHandler(this.PrevButton_Click);
            //
            // uxNextButton
            //
            this.uxNextButton.Location = new System.Drawing.Point(400, 3);
            this.uxNextButton.Name = "uxNextButton";
            this.uxNextButton.Size = new System.Drawing.Size(75, 23);
            this.uxNextButton.TabIndex = 2;
            this.uxNextButton.Text = "Next";
            this.uxNextButton.UseVisualStyleBackColor = true;
            this.uxNextButton.Click += new System.EventHandler(this.NextButton_Click);
            //
            // uxStartButton
            //
            this.uxStartButton.Location = new System.Drawing.Point(481, 3);
            this.uxStartButton.Name = "uxStartButton";
            this.uxStartButton.Size = new System.Drawing.Size(75, 23);
            this.uxStartButton.TabIndex = 3;
            this.uxStartButton.Text = "Start";
            this.uxStartButton.UseVisualStyleBackColor = true;
            this.uxStartButton.Click += new System.EventHandler(this.StartButton_Click);
            //
            // uxCancelButton
            //
            this.uxCancelButton.Location = new System.Drawing.Point(562, 3);
            this.uxCancelButton.Name = "uxCancelButton";
            this.uxCancelButton.Size = new System.Drawing.Size(75, 23);
            this.uxCancelButton.TabIndex = 2;
            this.uxCancelButton.Text = "Cancel";
            this.uxCancelButton.UseVisualStyleBackColor = true;
            this.uxCancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            //
            // uxBackgroundWorker
            //
            this.uxBackgroundWorker.WorkerReportsProgress = true;
            this.uxBackgroundWorker.WorkerSupportsCancellation = true;
            this.uxBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BackgroundWorker_DoWork);
            this.uxBackgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BackgroundWorker_RunWorkerCompleted);
            this.uxBackgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.BackgroundWorker_ProgressChanged);
            //
            // uxOpenFileDialog
            //
            this.uxOpenFileDialog.FileName = "openFileDialog1";
            //
            // uxTabs
            //
            this.uxTabs.Controls.Add(this.uxSourceTab);
            this.uxTabs.Controls.Add(this.uxDestTab);
            this.uxTabs.Controls.Add(this.uxOptionsPage);
            this.uxTabs.Controls.Add(this.uxProgressPage);
            this.uxTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxTabs.Location = new System.Drawing.Point(0, 46);
            this.uxTabs.Name = "uxTabs";
            this.uxTabs.SelectedIndex = 0;
            this.uxTabs.Size = new System.Drawing.Size(640, 337);
            this.uxTabs.TabIndex = 0;
            this.uxTabs.TabStop = false;
            //
            // uxSourceTab
            //
            this.uxSourceTab.Controls.Add(this.uxSourceGroupBox);
            this.uxSourceTab.Location = new System.Drawing.Point(4, 22);
            this.uxSourceTab.Name = "uxSourceTab";
            this.uxSourceTab.Padding = new System.Windows.Forms.Padding(6);
            this.uxSourceTab.Size = new System.Drawing.Size(632, 311);
            this.uxSourceTab.TabIndex = 0;
            this.uxSourceTab.Text = "-- source tab --";
            this.uxSourceTab.UseVisualStyleBackColor = true;
            //
            // uxSourceGroupBox
            //
            this.uxSourceGroupBox.Controls.Add(this.uxSourceLayout);
            this.uxSourceGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxSourceGroupBox.Location = new System.Drawing.Point(6, 6);
            this.uxSourceGroupBox.Name = "uxSourceGroupBox";
            this.uxSourceGroupBox.Padding = new System.Windows.Forms.Padding(6);
            this.uxSourceGroupBox.Size = new System.Drawing.Size(620, 299);
            this.uxSourceGroupBox.TabIndex = 4;
            this.uxSourceGroupBox.TabStop = false;
            this.uxSourceGroupBox.Text = "Select SOURCE device or image";
            //
            // uxSourceLayout
            //
            this.uxSourceLayout.ColumnCount = 3;
            this.uxSourceLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.uxSourceLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.uxSourceLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.uxSourceLayout.Controls.Add(this.uxSourceImageRadio, 0, 0);
            this.uxSourceLayout.Controls.Add(this.uxSourceDeviceRadio, 0, 2);
            this.uxSourceLayout.Controls.Add(this.uxSourceBrowseButton, 2, 1);
            this.uxSourceLayout.Controls.Add(this.uxSourceImageFileTextBox, 1, 1);
            this.uxSourceLayout.Controls.Add(this.uxSourceDeviceList, 1, 3);
            this.uxSourceLayout.Controls.Add(this.uxSourceRefreshButton, 2, 2);
            this.uxSourceLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxSourceLayout.Location = new System.Drawing.Point(6, 19);
            this.uxSourceLayout.Name = "uxSourceLayout";
            this.uxSourceLayout.RowCount = 4;
            this.uxSourceLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.uxSourceLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.uxSourceLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.uxSourceLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.uxSourceLayout.Size = new System.Drawing.Size(608, 274);
            this.uxSourceLayout.TabIndex = 2;
            //
            // uxSourceImageRadio
            //
            this.uxSourceImageRadio.AutoSize = true;
            this.uxSourceImageRadio.Checked = true;
            this.uxSourceLayout.SetColumnSpan(this.uxSourceImageRadio, 3);
            this.uxSourceImageRadio.Location = new System.Drawing.Point(3, 3);
            this.uxSourceImageRadio.Name = "uxSourceImageRadio";
            this.uxSourceImageRadio.Size = new System.Drawing.Size(148, 17);
            this.uxSourceImageRadio.TabIndex = 0;
            this.uxSourceImageRadio.TabStop = true;
            this.uxSourceImageRadio.Text = "Image file (in RAW format)";
            this.uxSourceImageRadio.UseVisualStyleBackColor = true;
            this.uxSourceImageRadio.CheckedChanged += new System.EventHandler(this.SourceImageRadio_CheckedChanged);
            //
            // uxSourceDeviceRadio
            //
            this.uxSourceDeviceRadio.AutoSize = true;
            this.uxSourceLayout.SetColumnSpan(this.uxSourceDeviceRadio, 2);
            this.uxSourceDeviceRadio.Location = new System.Drawing.Point(3, 55);
            this.uxSourceDeviceRadio.Name = "uxSourceDeviceRadio";
            this.uxSourceDeviceRadio.Size = new System.Drawing.Size(111, 17);
            this.uxSourceDeviceRadio.TabIndex = 1;
            this.uxSourceDeviceRadio.Text = "Device or partition";
            this.uxSourceDeviceRadio.UseVisualStyleBackColor = true;
            this.uxSourceDeviceRadio.CheckedChanged += new System.EventHandler(this.SourceDeviceRadio_CheckedChanged);
            //
            // uxSourceBrowseButton
            //
            this.uxSourceBrowseButton.Location = new System.Drawing.Point(530, 26);
            this.uxSourceBrowseButton.Name = "uxSourceBrowseButton";
            this.uxSourceBrowseButton.Size = new System.Drawing.Size(75, 23);
            this.uxSourceBrowseButton.TabIndex = 1;
            this.uxSourceBrowseButton.Text = "Browse";
            this.uxSourceBrowseButton.UseVisualStyleBackColor = true;
            this.uxSourceBrowseButton.Click += new System.EventHandler(this.SourceBrowseButton_Click);
            //
            // uxSourceImageFileTextBox
            //
            this.uxSourceImageFileTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxSourceImageFileTextBox.Location = new System.Drawing.Point(3, 26);
            this.uxSourceImageFileTextBox.Name = "uxSourceImageFileTextBox";
            this.uxSourceImageFileTextBox.Size = new System.Drawing.Size(521, 20);
            this.uxSourceImageFileTextBox.TabIndex = 2;
            //
            // uxSourceDeviceList
            //
            this.uxSourceDeviceList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.uxSourceNameColumnHeader,
            this.uxSourceSizeColumnHeader});
            this.uxSourceLayout.SetColumnSpan(this.uxSourceDeviceList, 2);
            this.uxSourceDeviceList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxSourceDeviceList.FullRowSelect = true;
            this.uxSourceDeviceList.HideSelection = false;
            this.uxSourceDeviceList.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1});
            this.uxSourceDeviceList.Location = new System.Drawing.Point(3, 84);
            this.uxSourceDeviceList.Name = "uxSourceDeviceList";
            this.uxSourceDeviceList.Size = new System.Drawing.Size(602, 187);
            this.uxSourceDeviceList.TabIndex = 3;
            this.uxSourceDeviceList.UseCompatibleStateImageBehavior = false;
            this.uxSourceDeviceList.View = System.Windows.Forms.View.Details;
            //
            // uxSourceNameColumnHeader
            //
            this.uxSourceNameColumnHeader.Text = "Name";
            this.uxSourceNameColumnHeader.Width = 394;
            //
            // uxSourceSizeColumnHeader
            //
            this.uxSourceSizeColumnHeader.Text = "Size";
            //
            // uxSourceRefreshButton
            //
            this.uxSourceRefreshButton.Location = new System.Drawing.Point(530, 55);
            this.uxSourceRefreshButton.Name = "uxSourceRefreshButton";
            this.uxSourceRefreshButton.Size = new System.Drawing.Size(75, 23);
            this.uxSourceRefreshButton.TabIndex = 1;
            this.uxSourceRefreshButton.Text = "Refresh";
            this.uxSourceRefreshButton.UseVisualStyleBackColor = true;
            this.uxSourceRefreshButton.Click += new System.EventHandler(this.SourceRefreshButton_Click);
            //
            // uxDestTab
            //
            this.uxDestTab.Controls.Add(this.uxDestGroupBox);
            this.uxDestTab.Location = new System.Drawing.Point(4, 22);
            this.uxDestTab.Name = "uxDestTab";
            this.uxDestTab.Padding = new System.Windows.Forms.Padding(6);
            this.uxDestTab.Size = new System.Drawing.Size(632, 311);
            this.uxDestTab.TabIndex = 1;
            this.uxDestTab.Text = "-- dest tab --";
            this.uxDestTab.UseVisualStyleBackColor = true;
            //
            // uxDestGroupBox
            //
            this.uxDestGroupBox.Controls.Add(this.uxDestLayout);
            this.uxDestGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxDestGroupBox.Location = new System.Drawing.Point(6, 6);
            this.uxDestGroupBox.Name = "uxDestGroupBox";
            this.uxDestGroupBox.Padding = new System.Windows.Forms.Padding(6);
            this.uxDestGroupBox.Size = new System.Drawing.Size(620, 299);
            this.uxDestGroupBox.TabIndex = 5;
            this.uxDestGroupBox.TabStop = false;
            this.uxDestGroupBox.Text = "Select TARGET device or image";
            //
            // uxDestLayout
            //
            this.uxDestLayout.ColumnCount = 3;
            this.uxDestLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.uxDestLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.uxDestLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.uxDestLayout.Controls.Add(this.uxDestImageRadio, 0, 0);
            this.uxDestLayout.Controls.Add(this.uxDestDeviceRadio, 0, 2);
            this.uxDestLayout.Controls.Add(this.uxDestBrowseButton, 2, 1);
            this.uxDestLayout.Controls.Add(this.uxDestImageFileTextBox, 1, 1);
            this.uxDestLayout.Controls.Add(this.uxDestDeviceList, 1, 3);
            this.uxDestLayout.Controls.Add(this.uxDestRefreshButton, 2, 2);
            this.uxDestLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxDestLayout.Location = new System.Drawing.Point(6, 19);
            this.uxDestLayout.Name = "uxDestLayout";
            this.uxDestLayout.RowCount = 4;
            this.uxDestLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.uxDestLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.uxDestLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.uxDestLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.uxDestLayout.Size = new System.Drawing.Size(608, 274);
            this.uxDestLayout.TabIndex = 2;
            //
            // uxDestImageRadio
            //
            this.uxDestImageRadio.AutoSize = true;
            this.uxDestLayout.SetColumnSpan(this.uxDestImageRadio, 3);
            this.uxDestImageRadio.Location = new System.Drawing.Point(3, 3);
            this.uxDestImageRadio.Name = "uxDestImageRadio";
            this.uxDestImageRadio.Size = new System.Drawing.Size(148, 17);
            this.uxDestImageRadio.TabIndex = 0;
            this.uxDestImageRadio.Text = "Image file (in RAW format)";
            this.uxDestImageRadio.UseVisualStyleBackColor = true;
            this.uxDestImageRadio.CheckedChanged += new System.EventHandler(this.DestImageRadio_CheckedChanged);
            //
            // uxDestDeviceRadio
            //
            this.uxDestDeviceRadio.AutoSize = true;
            this.uxDestDeviceRadio.Checked = true;
            this.uxDestLayout.SetColumnSpan(this.uxDestDeviceRadio, 2);
            this.uxDestDeviceRadio.Location = new System.Drawing.Point(3, 55);
            this.uxDestDeviceRadio.Name = "uxDestDeviceRadio";
            this.uxDestDeviceRadio.Size = new System.Drawing.Size(111, 17);
            this.uxDestDeviceRadio.TabIndex = 1;
            this.uxDestDeviceRadio.TabStop = true;
            this.uxDestDeviceRadio.Text = "Device or partition";
            this.uxDestDeviceRadio.UseVisualStyleBackColor = true;
            this.uxDestDeviceRadio.CheckedChanged += new System.EventHandler(this.DestDeviceRadio_CheckedChanged);
            //
            // uxDestBrowseButton
            //
            this.uxDestBrowseButton.Location = new System.Drawing.Point(530, 26);
            this.uxDestBrowseButton.Name = "uxDestBrowseButton";
            this.uxDestBrowseButton.Size = new System.Drawing.Size(75, 23);
            this.uxDestBrowseButton.TabIndex = 1;
            this.uxDestBrowseButton.Text = "Browse";
            this.uxDestBrowseButton.UseVisualStyleBackColor = true;
            this.uxDestBrowseButton.Click += new System.EventHandler(this.DestBrowseButton_Click);
            //
            // uxDestImageFileTextBox
            //
            this.uxDestImageFileTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxDestImageFileTextBox.Location = new System.Drawing.Point(3, 26);
            this.uxDestImageFileTextBox.Name = "uxDestImageFileTextBox";
            this.uxDestImageFileTextBox.Size = new System.Drawing.Size(521, 20);
            this.uxDestImageFileTextBox.TabIndex = 2;
            //
            // uxDestDeviceList
            //
            this.uxDestDeviceList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.uxDestNameColumnHeader,
            this.uxDestSizeColumnHeader});
            this.uxDestLayout.SetColumnSpan(this.uxDestDeviceList, 2);
            this.uxDestDeviceList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxDestDeviceList.FullRowSelect = true;
            this.uxDestDeviceList.HideSelection = false;
            this.uxDestDeviceList.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem2});
            this.uxDestDeviceList.Location = new System.Drawing.Point(3, 84);
            this.uxDestDeviceList.Name = "uxDestDeviceList";
            this.uxDestDeviceList.Size = new System.Drawing.Size(602, 187);
            this.uxDestDeviceList.TabIndex = 3;
            this.uxDestDeviceList.UseCompatibleStateImageBehavior = false;
            this.uxDestDeviceList.View = System.Windows.Forms.View.Details;
            //
            // uxDestNameColumnHeader
            //
            this.uxDestNameColumnHeader.Text = "Name";
            this.uxDestNameColumnHeader.Width = 394;
            //
            // uxDestSizeColumnHeader
            //
            this.uxDestSizeColumnHeader.Text = "Size";
            //
            // uxDestRefreshButton
            //
            this.uxDestRefreshButton.Location = new System.Drawing.Point(530, 55);
            this.uxDestRefreshButton.Name = "uxDestRefreshButton";
            this.uxDestRefreshButton.Size = new System.Drawing.Size(75, 23);
            this.uxDestRefreshButton.TabIndex = 1;
            this.uxDestRefreshButton.Text = "Refresh";
            this.uxDestRefreshButton.UseVisualStyleBackColor = true;
            this.uxDestRefreshButton.Click += new System.EventHandler(this.DestRefreshButton_Click);
            //
            // uxOptionsPage
            //
            this.uxOptionsPage.Controls.Add(this.uxOptionsGroupBox);
            this.uxOptionsPage.Location = new System.Drawing.Point(4, 22);
            this.uxOptionsPage.Name = "uxOptionsPage";
            this.uxOptionsPage.Padding = new System.Windows.Forms.Padding(6);
            this.uxOptionsPage.Size = new System.Drawing.Size(632, 311);
            this.uxOptionsPage.TabIndex = 2;
            this.uxOptionsPage.Text = "-- options tab --";
            this.uxOptionsPage.UseVisualStyleBackColor = true;
            //
            // uxOptionsGroupBox
            //
            this.uxOptionsGroupBox.Controls.Add(this.uxOptionsNotImplementedYetLabel);
            this.uxOptionsGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxOptionsGroupBox.Location = new System.Drawing.Point(6, 6);
            this.uxOptionsGroupBox.Name = "uxOptionsGroupBox";
            this.uxOptionsGroupBox.Size = new System.Drawing.Size(620, 299);
            this.uxOptionsGroupBox.TabIndex = 0;
            this.uxOptionsGroupBox.TabStop = false;
            this.uxOptionsGroupBox.Text = "Adjust process options";
            //
            // uxOptionsNotImplementedYetLabel
            //
            this.uxOptionsNotImplementedYetLabel.AutoSize = true;
            this.uxOptionsNotImplementedYetLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.uxOptionsNotImplementedYetLabel.Location = new System.Drawing.Point(3, 16);
            this.uxOptionsNotImplementedYetLabel.Name = "uxOptionsNotImplementedYetLabel";
            this.uxOptionsNotImplementedYetLabel.Size = new System.Drawing.Size(284, 13);
            this.uxOptionsNotImplementedYetLabel.TabIndex = 1;
            this.uxOptionsNotImplementedYetLabel.Text = "Sorry, there are no options available yet. Just press \"Next\".";
            //
            // uxProgressPage
            //
            this.uxProgressPage.Controls.Add(this.uxProgressGroupBox);
            this.uxProgressPage.Location = new System.Drawing.Point(4, 22);
            this.uxProgressPage.Name = "uxProgressPage";
            this.uxProgressPage.Padding = new System.Windows.Forms.Padding(6);
            this.uxProgressPage.Size = new System.Drawing.Size(632, 311);
            this.uxProgressPage.TabIndex = 3;
            this.uxProgressPage.Text = "-- progress tab --";
            this.uxProgressPage.UseVisualStyleBackColor = true;
            //
            // uxProgressGroupBox
            //
            this.uxProgressGroupBox.Controls.Add(this.uxProgressLayout);
            this.uxProgressGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxProgressGroupBox.Location = new System.Drawing.Point(6, 6);
            this.uxProgressGroupBox.Name = "uxProgressGroupBox";
            this.uxProgressGroupBox.Size = new System.Drawing.Size(620, 299);
            this.uxProgressGroupBox.TabIndex = 4;
            this.uxProgressGroupBox.TabStop = false;
            this.uxProgressGroupBox.Text = "Operation progress";
            //
            // uxProgressLayout
            //
            this.uxProgressLayout.AutoSize = true;
            this.uxProgressLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.uxProgressLayout.ColumnCount = 3;
            this.uxProgressLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.uxProgressLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.uxProgressLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.uxProgressLayout.Controls.Add(this.uxProgressSourceFileCaptionLabel, 0, 0);
            this.uxProgressLayout.Controls.Add(this.uxProgressMegabytesReadLabel, 1, 2);
            this.uxProgressLayout.Controls.Add(this.uxProgressBytesReadLabel, 2, 2);
            this.uxProgressLayout.Controls.Add(this.uxProgressTargetFileCaptionLabel, 0, 1);
            this.uxProgressLayout.Controls.Add(this.uxProgressDataReadCaption, 0, 2);
            this.uxProgressLayout.Controls.Add(this.uxProgressTargetFileLabel, 1, 1);
            this.uxProgressLayout.Controls.Add(this.uxProgressSourceFileLabel, 1, 0);
            this.uxProgressLayout.Controls.Add(this.uxProgressDataWrittenCaption, 0, 4);
            this.uxProgressLayout.Controls.Add(this.uxProgressMegabytesWrittenLabel, 1, 4);
            this.uxProgressLayout.Controls.Add(this.uxProgressBytesWrittenLabel, 2, 4);
            this.uxProgressLayout.Controls.Add(this.uxProgressStatusCaption, 0, 5);
            this.uxProgressLayout.Controls.Add(this.uxProgressStatusLabel, 1, 5);
            this.uxProgressLayout.Controls.Add(this.uxProgressErrorCaption, 0, 6);
            this.uxProgressLayout.Controls.Add(this.uxProgressErrorLabel, 1, 6);
            this.uxProgressLayout.Dock = System.Windows.Forms.DockStyle.Top;
            this.uxProgressLayout.Location = new System.Drawing.Point(3, 16);
            this.uxProgressLayout.Name = "uxProgressLayout";
            this.uxProgressLayout.RowCount = 7;
            this.uxProgressLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.uxProgressLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.uxProgressLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.uxProgressLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.uxProgressLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.uxProgressLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.uxProgressLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.uxProgressLayout.Size = new System.Drawing.Size(614, 108);
            this.uxProgressLayout.TabIndex = 5;
            //
            // uxProgressSourceFileCaptionLabel
            //
            this.uxProgressSourceFileCaptionLabel.AutoSize = true;
            this.uxProgressSourceFileCaptionLabel.Location = new System.Drawing.Point(3, 3);
            this.uxProgressSourceFileCaptionLabel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.uxProgressSourceFileCaptionLabel.Name = "uxProgressSourceFileCaptionLabel";
            this.uxProgressSourceFileCaptionLabel.Size = new System.Drawing.Size(60, 13);
            this.uxProgressSourceFileCaptionLabel.TabIndex = 2;
            this.uxProgressSourceFileCaptionLabel.Text = "Source file:";
            //
            // uxProgressMegabytesReadLabel
            //
            this.uxProgressMegabytesReadLabel.AutoSize = true;
            this.uxProgressMegabytesReadLabel.Location = new System.Drawing.Point(86, 35);
            this.uxProgressMegabytesReadLabel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.uxProgressMegabytesReadLabel.Name = "uxProgressMegabytesReadLabel";
            this.uxProgressMegabytesReadLabel.Size = new System.Drawing.Size(13, 13);
            this.uxProgressMegabytesReadLabel.TabIndex = 2;
            this.uxProgressMegabytesReadLabel.Text = "--";
            //
            // uxProgressBytesReadLabel
            //
            this.uxProgressBytesReadLabel.AutoSize = true;
            this.uxProgressBytesReadLabel.Location = new System.Drawing.Point(105, 35);
            this.uxProgressBytesReadLabel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.uxProgressBytesReadLabel.Name = "uxProgressBytesReadLabel";
            this.uxProgressBytesReadLabel.Size = new System.Drawing.Size(13, 13);
            this.uxProgressBytesReadLabel.TabIndex = 2;
            this.uxProgressBytesReadLabel.Text = "--";
            //
            // uxProgressTargetFileCaptionLabel
            //
            this.uxProgressTargetFileCaptionLabel.AutoSize = true;
            this.uxProgressTargetFileCaptionLabel.Location = new System.Drawing.Point(3, 19);
            this.uxProgressTargetFileCaptionLabel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.uxProgressTargetFileCaptionLabel.Name = "uxProgressTargetFileCaptionLabel";
            this.uxProgressTargetFileCaptionLabel.Size = new System.Drawing.Size(57, 13);
            this.uxProgressTargetFileCaptionLabel.TabIndex = 2;
            this.uxProgressTargetFileCaptionLabel.Text = "Target file:";
            //
            // uxProgressDataReadCaption
            //
            this.uxProgressDataReadCaption.AutoSize = true;
            this.uxProgressDataReadCaption.Location = new System.Drawing.Point(3, 35);
            this.uxProgressDataReadCaption.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.uxProgressDataReadCaption.Name = "uxProgressDataReadCaption";
            this.uxProgressDataReadCaption.Size = new System.Drawing.Size(57, 13);
            this.uxProgressDataReadCaption.TabIndex = 2;
            this.uxProgressDataReadCaption.Text = "Data read:";
            //
            // uxProgressTargetFileLabel
            //
            this.uxProgressTargetFileLabel.AutoSize = true;
            this.uxProgressLayout.SetColumnSpan(this.uxProgressTargetFileLabel, 2);
            this.uxProgressTargetFileLabel.Location = new System.Drawing.Point(86, 19);
            this.uxProgressTargetFileLabel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.uxProgressTargetFileLabel.Name = "uxProgressTargetFileLabel";
            this.uxProgressTargetFileLabel.Size = new System.Drawing.Size(68, 13);
            this.uxProgressTargetFileLabel.TabIndex = 2;
            this.uxProgressTargetFileLabel.Text = "-- target file --";
            //
            // uxProgressSourceFileLabel
            //
            this.uxProgressSourceFileLabel.AutoSize = true;
            this.uxProgressLayout.SetColumnSpan(this.uxProgressSourceFileLabel, 2);
            this.uxProgressSourceFileLabel.Location = new System.Drawing.Point(86, 3);
            this.uxProgressSourceFileLabel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.uxProgressSourceFileLabel.Name = "uxProgressSourceFileLabel";
            this.uxProgressSourceFileLabel.Size = new System.Drawing.Size(73, 13);
            this.uxProgressSourceFileLabel.TabIndex = 2;
            this.uxProgressSourceFileLabel.Text = "-- source file --";
            //
            // uxProgressDataWrittenCaption
            //
            this.uxProgressDataWrittenCaption.AutoSize = true;
            this.uxProgressDataWrittenCaption.Location = new System.Drawing.Point(3, 51);
            this.uxProgressDataWrittenCaption.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.uxProgressDataWrittenCaption.Name = "uxProgressDataWrittenCaption";
            this.uxProgressDataWrittenCaption.Size = new System.Drawing.Size(67, 13);
            this.uxProgressDataWrittenCaption.TabIndex = 2;
            this.uxProgressDataWrittenCaption.Text = "Data written:";
            //
            // uxProgressMegabytesWrittenLabel
            //
            this.uxProgressMegabytesWrittenLabel.AutoSize = true;
            this.uxProgressMegabytesWrittenLabel.Location = new System.Drawing.Point(86, 51);
            this.uxProgressMegabytesWrittenLabel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.uxProgressMegabytesWrittenLabel.Name = "uxProgressMegabytesWrittenLabel";
            this.uxProgressMegabytesWrittenLabel.Size = new System.Drawing.Size(13, 13);
            this.uxProgressMegabytesWrittenLabel.TabIndex = 2;
            this.uxProgressMegabytesWrittenLabel.Text = "--";
            //
            // uxProgressBytesWrittenLabel
            //
            this.uxProgressBytesWrittenLabel.AutoSize = true;
            this.uxProgressBytesWrittenLabel.Location = new System.Drawing.Point(105, 51);
            this.uxProgressBytesWrittenLabel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.uxProgressBytesWrittenLabel.Name = "uxProgressBytesWrittenLabel";
            this.uxProgressBytesWrittenLabel.Size = new System.Drawing.Size(13, 13);
            this.uxProgressBytesWrittenLabel.TabIndex = 2;
            this.uxProgressBytesWrittenLabel.Text = "--";
            //
            // uxProgressStatusCaption
            //
            this.uxProgressStatusCaption.AutoSize = true;
            this.uxProgressStatusCaption.Location = new System.Drawing.Point(3, 71);
            this.uxProgressStatusCaption.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.uxProgressStatusCaption.Name = "uxProgressStatusCaption";
            this.uxProgressStatusCaption.Size = new System.Drawing.Size(75, 13);
            this.uxProgressStatusCaption.TabIndex = 2;
            this.uxProgressStatusCaption.Text = "Current status:";
            //
            // uxProgressStatusLabel
            //
            this.uxProgressStatusLabel.AutoSize = true;
            this.uxProgressLayout.SetColumnSpan(this.uxProgressStatusLabel, 2);
            this.uxProgressStatusLabel.Location = new System.Drawing.Point(86, 71);
            this.uxProgressStatusLabel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.uxProgressStatusLabel.Name = "uxProgressStatusLabel";
            this.uxProgressStatusLabel.Size = new System.Drawing.Size(59, 13);
            this.uxProgressStatusLabel.TabIndex = 2;
            this.uxProgressStatusLabel.Text = "In progress";
            //
            // uxProgressErrorCaption
            //
            this.uxProgressErrorCaption.AutoSize = true;
            this.uxProgressErrorCaption.Location = new System.Drawing.Point(3, 91);
            this.uxProgressErrorCaption.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.uxProgressErrorCaption.Name = "uxProgressErrorCaption";
            this.uxProgressErrorCaption.Size = new System.Drawing.Size(77, 13);
            this.uxProgressErrorCaption.TabIndex = 2;
            this.uxProgressErrorCaption.Text = "Error message:";
            //
            // uxProgressErrorLabel
            //
            this.uxProgressErrorLabel.AutoSize = true;
            this.uxProgressLayout.SetColumnSpan(this.uxProgressErrorLabel, 2);
            this.uxProgressErrorLabel.Location = new System.Drawing.Point(86, 91);
            this.uxProgressErrorLabel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.uxProgressErrorLabel.Name = "uxProgressErrorLabel";
            this.uxProgressErrorLabel.Size = new System.Drawing.Size(33, 13);
            this.uxProgressErrorLabel.TabIndex = 2;
            this.uxProgressErrorLabel.Text = "None";
            //
            // MainForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(640, 412);
            this.Controls.Add(this.uxTabs);
            this.Controls.Add(this.uxStepsLayout);
            this.Controls.Add(this.uxBottomLayout);
            this.Name = "MainForm";
            this.Text = "DDwizard";
            this.uxStepsLayout.ResumeLayout(false);
            this.uxStepsLayout.PerformLayout();
            this.uxBottomLayout.ResumeLayout(false);
            this.uxBottomLayout.PerformLayout();
            this.uxTabs.ResumeLayout(false);
            this.uxSourceTab.ResumeLayout(false);
            this.uxSourceGroupBox.ResumeLayout(false);
            this.uxSourceLayout.ResumeLayout(false);
            this.uxSourceLayout.PerformLayout();
            this.uxDestTab.ResumeLayout(false);
            this.uxDestGroupBox.ResumeLayout(false);
            this.uxDestLayout.ResumeLayout(false);
            this.uxDestLayout.PerformLayout();
            this.uxOptionsPage.ResumeLayout(false);
            this.uxOptionsGroupBox.ResumeLayout(false);
            this.uxOptionsGroupBox.PerformLayout();
            this.uxProgressPage.ResumeLayout(false);
            this.uxProgressGroupBox.ResumeLayout(false);
            this.uxProgressGroupBox.PerformLayout();
            this.uxProgressLayout.ResumeLayout(false);
            this.uxProgressLayout.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button uxStepSourceButton;
        private System.Windows.Forms.Button uxStepDestButton;
        private System.Windows.Forms.Button uxStepOptionsButton;
        private System.Windows.Forms.Button uxStepProgressButton;
        private System.Windows.Forms.FlowLayoutPanel uxStepsLayout;
        private System.Windows.Forms.GroupBox uxSourceGroupBox;
        private WizardTabControl uxTabs;
        private System.Windows.Forms.TabPage uxSourceTab;
        private System.Windows.Forms.TabPage uxDestTab;
        private System.Windows.Forms.TabPage uxOptionsPage;
        private System.Windows.Forms.TabPage uxProgressPage;
        private System.Windows.Forms.TableLayoutPanel uxSourceLayout;
        private System.Windows.Forms.RadioButton uxSourceImageRadio;
        private System.Windows.Forms.RadioButton uxSourceDeviceRadio;
        private System.Windows.Forms.TextBox uxSourceImageFileTextBox;
        private System.Windows.Forms.Button uxSourceBrowseButton;
        private System.Windows.Forms.ListView uxSourceDeviceList;
        private System.Windows.Forms.ColumnHeader uxSourceNameColumnHeader;
        private System.Windows.Forms.ColumnHeader uxSourceSizeColumnHeader;
        private System.Windows.Forms.Button uxSourceRefreshButton;
        private System.Windows.Forms.GroupBox uxDestGroupBox;
        private System.Windows.Forms.TableLayoutPanel uxDestLayout;
        private System.Windows.Forms.RadioButton uxDestImageRadio;
        private System.Windows.Forms.RadioButton uxDestDeviceRadio;
        private System.Windows.Forms.Button uxDestBrowseButton;
        private System.Windows.Forms.TextBox uxDestImageFileTextBox;
        private System.Windows.Forms.ListView uxDestDeviceList;
        private System.Windows.Forms.ColumnHeader uxDestNameColumnHeader;
        private System.Windows.Forms.ColumnHeader uxDestSizeColumnHeader;
        private System.Windows.Forms.Button uxDestRefreshButton;
        private System.Windows.Forms.Label uxOptionsNotImplementedYetLabel;
        private System.Windows.Forms.GroupBox uxOptionsGroupBox;
        private System.Windows.Forms.GroupBox uxProgressGroupBox;
        private System.Windows.Forms.Label uxProgressBytesReadLabel;
        private System.Windows.Forms.Label uxProgressMegabytesReadLabel;
        private System.Windows.Forms.Label uxProgressSourceFileCaptionLabel;
        private System.Windows.Forms.TableLayoutPanel uxProgressLayout;
        private System.Windows.Forms.Label uxProgressTargetFileCaptionLabel;
        private System.Windows.Forms.Label uxProgressDataReadCaption;
        private System.Windows.Forms.Label uxProgressTargetFileLabel;
        private System.Windows.Forms.Label uxProgressSourceFileLabel;
        private System.Windows.Forms.TableLayoutPanel uxBottomLayout;
        private System.Windows.Forms.LinkLabel uxAboutLinkLabel;
        private System.Windows.Forms.Button uxPrevButton;
        private System.Windows.Forms.Button uxNextButton;
        private System.Windows.Forms.Button uxStartButton;
        private System.Windows.Forms.Button uxCancelButton;
        private System.ComponentModel.BackgroundWorker uxBackgroundWorker;
        private System.Windows.Forms.OpenFileDialog uxOpenFileDialog;
        private System.Windows.Forms.SaveFileDialog uxSaveFileDialog;
        private System.Windows.Forms.Label uxProgressDataWrittenCaption;
        private System.Windows.Forms.Label uxProgressMegabytesWrittenLabel;
        private System.Windows.Forms.Label uxProgressBytesWrittenLabel;
        private System.Windows.Forms.Label uxProgressStatusCaption;
        private System.Windows.Forms.Label uxProgressStatusLabel;
        private System.Windows.Forms.Label uxProgressErrorCaption;
        private System.Windows.Forms.Label uxProgressErrorLabel;
    }
}
